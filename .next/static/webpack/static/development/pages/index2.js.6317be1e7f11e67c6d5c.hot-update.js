webpackHotUpdate("static\\development\\pages\\index2.js",{

/***/ "./pages/index2.js":
/*!*************************!*\
  !*** ./pages/index2.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/regenerator */ "./node_modules/@babel/runtime-corejs2/regenerator/index.js");
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "../node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _images_sideImage_png__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./images/sideImage.png */ "./pages/images/sideImage.png");
/* harmony import */ var _images_sideImage_png__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_images_sideImage_png__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _images_mobile_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./images/mobile.png */ "./pages/images/mobile.png");
/* harmony import */ var _images_mobile_png__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_images_mobile_png__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _images_appstore_jpg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./images/appstore.jpg */ "./pages/images/appstore.jpg");
/* harmony import */ var _images_appstore_jpg__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_images_appstore_jpg__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _images_googleplay_jpg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./images/googleplay.jpg */ "./pages/images/googleplay.jpg");
/* harmony import */ var _images_googleplay_jpg__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_images_googleplay_jpg__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _images_fb_png__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./images/fb.png */ "./pages/images/fb.png");
/* harmony import */ var _images_fb_png__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_images_fb_png__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _images_google_png__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./images/google.png */ "./pages/images/google.png");
/* harmony import */ var _images_google_png__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_images_google_png__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! isomorphic-unfetch */ "./node_modules/next/dist/build/polyfills/fetch/index.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! next/head */ "./node_modules/next/dist/next-server/lib/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_9__);

var _jsxFileName = "C:\\xampp\\htdocs\\design\\pages\\index2.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;









var index2 = function index2(props) {
  console.warn('props', props);
  return __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  }, __jsx(next_head__WEBPACK_IMPORTED_MODULE_9___default.a, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }, __jsx("title", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17
    },
    __self: this
  }, "Next js"), __jsx("link", {
    rel: "stylesheet",
    href: "https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    },
    __self: this
  }, "        "), __jsx("link", {
    rel: "stylesheet",
    href: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    },
    __self: this
  })), __jsx("div", {
    className: "container",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }, __jsx("div", {
    className: "row",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24
    },
    __self: this
  }, __jsx("div", {
    className: "sc-hzDkRC bIAQsw",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    },
    __self: this
  }, __jsx("div", {
    className: "sc-bdVaJa fXmDiD",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: this
  }, __jsx("div", {
    className: "sc-bwzfXH fqNEzu",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28
    },
    __self: this
  }, __jsx("img", {
    src: _images_sideImage_png__WEBPACK_IMPORTED_MODULE_2___default.a,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29
    },
    __self: this
  }))), __jsx("div", {
    className: "sc-jhAzac MUmtV",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34
    },
    __self: this
  }, __jsx("div", {
    className: "sc-iRbamj ecBduw",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    },
    __self: this
  }, __jsx("div", {
    className: "sc-dxgOiQ jDkJnf",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36
    },
    __self: this
  }, __jsx("div", {
    className: "sc-ckVGcZ pERxi",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37
    },
    __self: this
  }, __jsx("img", {
    src: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALYAAAAZCAYAAACcutQ/AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MjEyNEQ0OEM1MDI2MTFFQTkyMjZGNkQzNEE5QUQ3RUIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MjEyNEQ0OEQ1MDI2MTFFQTkyMjZGNkQzNEE5QUQ3RUIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoyMTI0RDQ4QTUwMjYxMUVBOTIyNkY2RDM0QTlBRDdFQiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoyMTI0RDQ4QjUwMjYxMUVBOTIyNkY2RDM0QTlBRDdFQiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PjrBjAsAAAeFSURBVHja7Ft9TFVlGH+OVxGjC1e8MExI0UyUwlhhRGU2TNE1xzS/8w+dNl1TWdLK1mDyT1tiU2fNhR+bTFc2k5qpoDW1BZgzygJpTtGAvLvervfyJcjH6Twvvodzz33Px/1CwPPb3p2v57z3cPi9z/k9v/ccjud5GOp4or7e649MHzkSvo2N5bTOPQa/843gAgODA2YIh9WQzg0zboU6DFIPLjRDO1yEm/xwX7LcvwkJnHHrDAx0NMBdMDK2Ck7AX7xxFwYfOqDbILb6Deo0bsIghANaDGIbGJoYHoi7oKS5810uvry9Hao7PTNevMkEGeHhsDM6WrdWf8Nm4+X9ZI0aBQesVk6tHngvMhJyo6KMmuARRVAzNhIaSVbU3OxFaiLqu7vhaGsrISLGahEa41j9nL53T1cfBoyMHTAW2e18RUeH7ngkv5DV+TNxcV5ZNamhgW/S4a9jH/VdXQa5DYQmY+c4nT6RmgKzMQ4I+QBp8mHSCLN3KKtrA49wxkZ5wcK0ESNqBD28W1itEwi4tqazc7E8Rj4gtAaImeOguR9mS3HGEatrLdT9WgPF7xZ67EtbmgnzcleG9PpOFR6GS1//CLM3LYGMVVkGk+XEZhVeSmAVZKsdDl4lNpluC+tlSoUe6mmUJEp9KRWqUwTJEmySn4Fa/irYfCKXgQFI7LVmM+wTtKq/KGVIAXwPQ8mRELI41MgKQloglirICiX35e/4eM6XgamF3XBOd1/lxadFUmcXrIOUeS95HAs18IkQ6qfCoCZ2gcXCuXt6+G8U5IQ/eDYsTPHYM8KxGobTUeh280oDQQ04iCr90PcsbIJZnB5yt95tgrO7jzJJjZBKA/v1Bti7LM/j+JaynRAxOpKsXzlVASV5RTD55RTI3PiWGEslhvSpsOrzXEicMY0pRZT6we3lO3OY14PHkue+KJ4njUM4/7HBz/tPwB8ny8l21pYVMGPZbLJ+v60dKg+XQVXJBXDbnTBuaiJkbloME15IEs/XE/Nd/j6yxH7/PFkBlV+dIXHzt66CsVMnBKaxd0VHc+gu+EmQdqGFy90KoVGC4FtEbqGNV+ukvptdqCWrDBJCoiASm5L7GFTxjeSS2bBfaxDXJ6UnK8ZRssmxY06OB7kR1365QhoFDpxbl2s99qGWz7t0QPX65f3gOl4HDj75IJPHSoGkLN5QSAg5OSOF7Lt99WZfDbJ1L1wr7zu38WodHNrwKSzdvhGmzErVHUMHjaPuNjlO447m7oHNPxQG7orgK5xa2fEBiX2FRYvUhNhdXcz9CSaT6nkJw4PmWPY5M5DKIcHDwBRQP5TUmFWRkNgwM5J/6Ilyr3gku5y4uI2ZWvq00ALth/5WY/UNsrx8/LyYqen1YKHLwn+3bCKpl+/KIW3eB2/3SsBzVYSwUbHRsO5QHuln1jvZvS7V9sO6Yzz+j9Ofgg/PfwHZ+WvJNv42PjGCYvedFQo4DXKH69mHfaBE8KVh5lWyBP0ZEMFADDzu97lSAk5/M6PvCSQ8+hGYjaVAstEMTglJY2Mnx/f161QntrSf8c8neRxzNdzplW8r5oj7kmamMvsZGdH7b0VyXij6Hlocbgh7LNzj2lOzZ4pyIX3lHA9C6omR4unXUkn/KZJ71dHa7vf990p3SO5A3Qac8vZnOvuzpiavH62+f1/1nIogyhC9iBjTJyGuV1Z7aeyhgOgn40iGPfdlidgwmyLx7jX11mOWsVYxnpKeElJPDGsgBQvDlNwGvR2grxxKsuE0vFJh+bCIHTspXsyqKDfQy5bbgFL9LJUd1aUXmdm0P2CJjyHL2gtV4r7KI2WK8TPXLSB6ePr83ixasm0fydyiLm1p89DkTO2qIyYkrogvwSwS4UtNcpsO43YI2XdLZKQH69c4HDxrpnCukOEPWq0cLlmWn5DJcVKG32axePSHU+8PK6MtyF9DikBa1ElBdSs6Jkh8LASpi8KSJ/0FlB3opNCmBiQhZlgs8rC1uVqILGm+cxcS06aRog9djOcWvELian/6jZyHmhqlh56YAUVsqW+M/jISkuUl73C74XRbG4/Ed/f04HshJPuycPDBm3pKfTGclocOzMhYELEmacYlTyRLlCjmMVFexNdyNkIFtArpYKMDEK+V5dxg8Xjyk2JIW5JJsi4tBMeMjwNzzGiyju5F8frtYE0cK7obr29YSJYT05M1Y0IJTulj3oV2u6b9RydO8lwu3t9JnsUREcRupNs4+1gahPc/pLOk/n7Mi1/Q3AAHDFVggUufOvKpeSzu9iz6SNxGkma9v1K06fD48Y+LRIuO1FYSn1tvTEHaGrJE54RmcdY+X2CFCGViKxGCRWzEZqfT50keJXLNttn4mk59X6/gNWi9jx3IV+q+zEYOdLAmi9SeIihHMHMjlAiG5MVCEDO5tDj0NSaYGAdR6lIESaPXIcGsm2Ay8aiHfc2oLGfGlyeGAf+h9sIWklArY6J7osdh6U/EgFk9Y8uliXyfUsbLe/AFjTzror+NmrtAVgSqgSVN5PKFdW1LhRihcUrHcWpfz3UcgUu8A1qNETBIgJNq6+FV7n8BBgACbCaQ0Z+YewAAAABJRU5ErkJggg==',
    alt: "picture",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37
    },
    __self: this
  }))), __jsx("div", {
    "class": "sc-fBuWsC dXbTGN",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: this
  }, __jsx("img", {
    src: _images_mobile_png__WEBPACK_IMPORTED_MODULE_3___default.a,
    className: "img-fluid",
    width: "100%",
    alt: "logo",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: this
  })), __jsx("div", {
    className: "container",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41
    },
    __self: this
  }, __jsx("div", {
    className: "row",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42
    },
    __self: this
  }, __jsx("div", {
    className: "col-lg-4 col-md-4 col-10",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: this
  }, __jsx("h1", {
    className: "sc-jKJlTe johnthan",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44
    },
    __self: this
  }, "Jonathon, Wellcome to"), __jsx("h2", {
    className: "update",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44
    },
    __self: this
  }, "Update your profile picture and create a   "))), __jsx("div", {
    className: "container",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47
    },
    __self: this
  }, __jsx("div", {
    className: "row",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48
    },
    __self: this
  }, __jsx("div", {
    className: "col-lg-4 col-md-4 col-sm-4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49
    },
    __self: this
  }, __jsx("input", {
    type: "password",
    className: " fullname",
    id: "email",
    placeholder: "Create Password",
    name: "email/",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50
    },
    __self: this
  })), __jsx("div", {
    className: "col-lg-4 col-md-4 col-sm-4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52
    },
    __self: this
  }, __jsx("input", {
    type: "text",
    className: " fullname",
    id: "email",
    placeholder: "Mobile Number (optional)",
    name: "email/",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53
    },
    __self: this
  })), __jsx("div", {
    className: "col-lg-6 col-md-6 col-sm-4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55
    },
    __self: this
  }, __jsx("input", {
    type: "text",
    className: " fullname",
    id: "email",
    placeholder: "How Do you Identify?",
    name: "email/",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56
    },
    __self: this
  })), __jsx("div", {
    className: "col-lg-4 col-md-4 col-sm-4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58
    },
    __self: this
  }, __jsx("input", {
    type: "text",
    className: " fullname",
    id: "email",
    placeholder: "Mobile Number (optional)",
    name: "email/",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59
    },
    __self: this
  })), __jsx("div", {
    className: "col-lg-6 col-md-4 col-sm-2",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 61
    },
    __self: this
  }, __jsx("button", {
    type: "button",
    className: "btn btn-primary btn-lg btn-bloc stepInside ",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62
    },
    __self: this
  }, "Step Inside"))))), __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 67
    },
    __self: this
  })))))));
};

index2.getInitialProps = function _callee() {
  var data, result;
  return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_8___default()('https://jsonplaceholder.typicode.com/todos/1'));

        case 2:
          data = _context.sent;
          _context.next = 5;
          return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(data.json());

        case 5:
          result = _context.sent;
          return _context.abrupt("return", {
            posts: result
          });

        case 7:
        case "end":
          return _context.stop();
      }
    }
  });
};

/* harmony default export */ __webpack_exports__["default"] = (index2);

/***/ })

})
//# sourceMappingURL=index2.js.6317be1e7f11e67c6d5c.hot-update.js.map