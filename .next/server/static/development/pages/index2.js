module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./pages/images/appstore.jpg":
/*!***********************************!*\
  !*** ./pages/images/appstore.jpg ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/appstore-a5533aa87b3debd087c14ba55428339e.jpg";

/***/ }),

/***/ "./pages/images/fb.png":
/*!*****************************!*\
  !*** ./pages/images/fb.png ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUUAAAFFCAMAAABMqg9vAAACH1BMVEUAAABGaLlCaLJDZ7REabNVgNVCaLJDaLJDZ7NDaLL///9CaLNDZ7NEabRDabJCZ7JJbbZDaLNDZ7VDaLNDaLNDZ7JFabNcfL1Mb7b////6+/2esdivv99lg8Cfstj9/v/e5fK/zOX5+v1ohcL3+fxGarSSp9PP2OzP2eykttrv8vlUdbl3kshEabNLbrZPcrh6lMlFarSBmsysvd79/f6PpdLa4fCqutxQcrj+/v9yjsZ7lclae7yKodD8/f7V3u7Dz+fj6fRohsJvi8X09vvO2OtRc7nu8fjs8Pi1w+FwjMX3+Px9l8qxwN9jgsCrvN2hs9mHn8+8yeTk6fS0w+FtisROcLfF0OhTdblKbbZzjsbZ4fCHns5OcbesvN1Zery3xeJxjcbp7fZRc7iIoM+2xOJhgL/J0+lefr7K1epYebv2+PzN1+uYrdZkgsBUdrrf5fLI0+l/mMtnhcH19/vd4/HE0OeUqdR8lspNcLfy9fp5lMmnudxff77V3e6No9F0j8fq7ve6x+OitNmKoc+4xuK7yeSXq9V8lcru8vlticRCZ7Lp7vaYrNVWd7pJbbW4xuPx9PlXeLvS2+1DaLPn7PawwN/R2uzj6PTc4/HY4O+EnM2brtb7/P7CzubX3+/H0ul4ksiNpNHg5vPr7/eCm8xigb+VqtS9yuTh5/Pt8fhsicPM1uuyweCoudzJ1OnAzebm6/Xo7PZIbLWgs9m0EJVeAAAAFnRSTlMAFttjWgbE/fXOAaXDRJzBFd1Fwp3eQRdNjQAABPlJREFUeF7s01VqBGEQReHb6jKz7HGfuLvrAgOBhB8iJF2v51vAfThUyeUFReOng98h9Zsi8PStPIyTPw8hicNcX1TZP2eQVXJJUd1iBXUkR6dsNYKy40TsthxB9zNj1P4SUX48dW0YQa13lWkElSTlmWkDWS4pNI4glBQbNxBLXmLcQOIpMI8gUGHeQKHGvIFGvnkDvlLzBlIN7OBUBBWpSEVQkYpUpCKoSEUqUhFUpCIVqQgqUpGKVAQVqUhFUJGKVKQiqEhFKlIRVFyO+zu7d/sPG9snq96sd306fH662jpY3B4+Ttf3r6Pjs6P++XxCxZ+83Fxcbu69tXNfP1YVQACHz93eAAXc5WxBel9qL/bee++99957y9VodDeWBBAlWJ6E9Q8UcQPsXRO5Z+bBkO/3J3wP8zDJTHlSDV66YMXC4fk/7Vq8eoziMcFdV209u6zUIMWjrbvu3CVl9SjW6xs2XrS5DEXx1qEDZSyKIx/vCQpS/PqrHUE/ivNWLoryUTxrVRSP4sW3hO0o7r8kKkdxYjgMR/GBc8JuFAfOC7NRPP+zsBrF/UvCaBQvGAybUTx4YZiM4uPx/Q3F7b+FwSiOPRkHo/hU3Ivi4j0Uw932XZyL4mVxLYqXx7Eojt1BMd6dcSuKXx6gGG8oTkXxrisoxvsiLkVxdJxivCvLeBSXU4x3dRmP4jUUEzpEMd5IGY/itRQT+p5ivN1lPIrLKCZ0PcWEbqAYb3sMaNG+G3/ZeNPA6psn+vtHGxqbrH7qK64JEI5vu32p++i/+7Oy4fL1o5MAFJ+raHjo0+MAFD+vhrh33nEAihvOrIR494kAFO+phHjvFACK91VBvH8txSk9WEVx/VQAivMrIP7RAEDxoQqKD1Ns6JHmERc0AlD8tnnFIYqN/dC84qMUG/u5acTD0wAojjet+BjFaTV/xruJ4rSeaFpxC8VpNb+M2ElxWmXTPZ2ARHEgAYniMwlIFDOW3BT7E5Ao1k81RYoUKVKkSJEiRYoUKVKkSJEiRYoUKVKkSJEiRYoUKVKkSJEiRYoUKVKkSLH8P/UsxYR+pJjQMMWEPqGY0PMUE3qBYkLrKMZ7sU4x3ksUE3qZYkKbKCb0CsWEXqWY0AjFhF6jGO/1OsV4WykmtJdiQtsoJrSMYkJvUEzoIMWE1lKMt6pOMd4+igktpJjQmxQT2kIxoTUUE3qLYkITFOP9XqcY722KCf1KMaGVFBN6h2JC71JMaIBivB1LKcZ7r04x3jcUE3qfYkIfUEzoQ4oJ7aSY0G6K8Tb3U4x3uE4x3gqKCX3k44GPB01EkSJFihQpUqRIkSJFihQpUqRIkSJFihQpUqRIkSJFihQpUqRIkSJFihQpUqRIkSJFihQpUqRIkSJFihQpUqRIkSJFihQpUqRIkSJFihQpUqRIkSJFihQpUqRIkSJFihQpUqRIkSJFihQpUqRIkSJFihQpUqRIkSJFihQpUqRIkSJFihQpUqRIkSJFihQpUqRIkSJFil0Uw3UVLRTDtRSzKIabVfRSDNdbtFIM11rUOikG66wVRQfFYB1FUbRRDNZ2RLGnm2Ko7p7iSLMphppRHG0uxUAzi39q76NYub72YrLaGRQrdvppxbFqfRQr1VcrTqh9LsUKzWwvpja7m2KTdc8optXT1tFJ8aTr7GjrKf61WmvvnJYuiv9RV8uc3tYpA/EvV2zqGrDuJHYAAAAASUVORK5CYII="

/***/ }),

/***/ "./pages/images/google.png":
/*!*********************************!*\
  !*** ./pages/images/google.png ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/google-2052fbd1d7526ebc4788704d3042778c.png";

/***/ }),

/***/ "./pages/images/googleplay.jpg":
/*!*************************************!*\
  !*** ./pages/images/googleplay.jpg ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/googleplay-4223b9a771b6bb04142e299fc053a2fe.jpg";

/***/ }),

/***/ "./pages/images/mobile.png":
/*!*********************************!*\
  !*** ./pages/images/mobile.png ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/mobile-f587ebaf96a3e1bebb3965f9e3e2e4b2.png";

/***/ }),

/***/ "./pages/images/sideImage.png":
/*!************************************!*\
  !*** ./pages/images/sideImage.png ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/images/sideImage-295281fb7033892a85dfb1f9555c6da5.png";

/***/ }),

/***/ "./pages/index2.js":
/*!*************************!*\
  !*** ./pages/index2.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _images_sideImage_png__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./images/sideImage.png */ "./pages/images/sideImage.png");
/* harmony import */ var _images_sideImage_png__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_images_sideImage_png__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _images_mobile_png__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./images/mobile.png */ "./pages/images/mobile.png");
/* harmony import */ var _images_mobile_png__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_images_mobile_png__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _images_appstore_jpg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./images/appstore.jpg */ "./pages/images/appstore.jpg");
/* harmony import */ var _images_appstore_jpg__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_images_appstore_jpg__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _images_googleplay_jpg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./images/googleplay.jpg */ "./pages/images/googleplay.jpg");
/* harmony import */ var _images_googleplay_jpg__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_images_googleplay_jpg__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _images_fb_png__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./images/fb.png */ "./pages/images/fb.png");
/* harmony import */ var _images_fb_png__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_images_fb_png__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _images_google_png__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./images/google.png */ "./pages/images/google.png");
/* harmony import */ var _images_google_png__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_images_google_png__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! isomorphic-unfetch */ "isomorphic-unfetch");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_8__);
var _jsxFileName = "C:\\xampp\\htdocs\\design\\pages\\index2.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;









const index2 = props => {
  console.warn('props', props);
  return __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: undefined
  }, __jsx(next_head__WEBPACK_IMPORTED_MODULE_8___default.a, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: undefined
  }, __jsx("title", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17
    },
    __self: undefined
  }, "Next js"), __jsx("link", {
    rel: "stylesheet",
    href: "https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    },
    __self: undefined
  }, "        "), __jsx("link", {
    rel: "stylesheet",
    href: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    },
    __self: undefined
  })), __jsx("div", {
    className: "container",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: undefined
  }, __jsx("div", {
    className: "row",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24
    },
    __self: undefined
  }, __jsx("div", {
    className: "sc-hzDkRC bIAQsw",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    },
    __self: undefined
  }, __jsx("div", {
    className: "sc-bdVaJa fXmDiD",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: undefined
  }, __jsx("div", {
    className: "sc-bwzfXH fqNEzu",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28
    },
    __self: undefined
  }, __jsx("img", {
    src: _images_sideImage_png__WEBPACK_IMPORTED_MODULE_1___default.a,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29
    },
    __self: undefined
  }))), __jsx("div", {
    className: "sc-jhAzac MUmtV",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34
    },
    __self: undefined
  }, __jsx("div", {
    className: "sc-iRbamj ecBduw",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    },
    __self: undefined
  }, __jsx("div", {
    className: "sc-dxgOiQ jDkJnf",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36
    },
    __self: undefined
  }, __jsx("div", {
    className: "sc-ckVGcZ pERxi",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37
    },
    __self: undefined
  }, __jsx("img", {
    src: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALYAAAAZCAYAAACcutQ/AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MjEyNEQ0OEM1MDI2MTFFQTkyMjZGNkQzNEE5QUQ3RUIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MjEyNEQ0OEQ1MDI2MTFFQTkyMjZGNkQzNEE5QUQ3RUIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoyMTI0RDQ4QTUwMjYxMUVBOTIyNkY2RDM0QTlBRDdFQiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoyMTI0RDQ4QjUwMjYxMUVBOTIyNkY2RDM0QTlBRDdFQiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PjrBjAsAAAeFSURBVHja7Ft9TFVlGH+OVxGjC1e8MExI0UyUwlhhRGU2TNE1xzS/8w+dNl1TWdLK1mDyT1tiU2fNhR+bTFc2k5qpoDW1BZgzygJpTtGAvLvervfyJcjH6Twvvodzz33Px/1CwPPb3p2v57z3cPi9z/k9v/ccjud5GOp4or7e649MHzkSvo2N5bTOPQa/843gAgODA2YIh9WQzg0zboU6DFIPLjRDO1yEm/xwX7LcvwkJnHHrDAx0NMBdMDK2Ck7AX7xxFwYfOqDbILb6Deo0bsIghANaDGIbGJoYHoi7oKS5810uvry9Hao7PTNevMkEGeHhsDM6WrdWf8Nm4+X9ZI0aBQesVk6tHngvMhJyo6KMmuARRVAzNhIaSVbU3OxFaiLqu7vhaGsrISLGahEa41j9nL53T1cfBoyMHTAW2e18RUeH7ngkv5DV+TNxcV5ZNamhgW/S4a9jH/VdXQa5DYQmY+c4nT6RmgKzMQ4I+QBp8mHSCLN3KKtrA49wxkZ5wcK0ESNqBD28W1itEwi4tqazc7E8Rj4gtAaImeOguR9mS3HGEatrLdT9WgPF7xZ67EtbmgnzcleG9PpOFR6GS1//CLM3LYGMVVkGk+XEZhVeSmAVZKsdDl4lNpluC+tlSoUe6mmUJEp9KRWqUwTJEmySn4Fa/irYfCKXgQFI7LVmM+wTtKq/KGVIAXwPQ8mRELI41MgKQloglirICiX35e/4eM6XgamF3XBOd1/lxadFUmcXrIOUeS95HAs18IkQ6qfCoCZ2gcXCuXt6+G8U5IQ/eDYsTPHYM8KxGobTUeh280oDQQ04iCr90PcsbIJZnB5yt95tgrO7jzJJjZBKA/v1Bti7LM/j+JaynRAxOpKsXzlVASV5RTD55RTI3PiWGEslhvSpsOrzXEicMY0pRZT6we3lO3OY14PHkue+KJ4njUM4/7HBz/tPwB8ny8l21pYVMGPZbLJ+v60dKg+XQVXJBXDbnTBuaiJkbloME15IEs/XE/Nd/j6yxH7/PFkBlV+dIXHzt66CsVMnBKaxd0VHc+gu+EmQdqGFy90KoVGC4FtEbqGNV+ukvptdqCWrDBJCoiASm5L7GFTxjeSS2bBfaxDXJ6UnK8ZRssmxY06OB7kR1365QhoFDpxbl2s99qGWz7t0QPX65f3gOl4HDj75IJPHSoGkLN5QSAg5OSOF7Lt99WZfDbJ1L1wr7zu38WodHNrwKSzdvhGmzErVHUMHjaPuNjlO447m7oHNPxQG7orgK5xa2fEBiX2FRYvUhNhdXcz9CSaT6nkJw4PmWPY5M5DKIcHDwBRQP5TUmFWRkNgwM5J/6Ilyr3gku5y4uI2ZWvq00ALth/5WY/UNsrx8/LyYqen1YKHLwn+3bCKpl+/KIW3eB2/3SsBzVYSwUbHRsO5QHuln1jvZvS7V9sO6Yzz+j9Ofgg/PfwHZ+WvJNv42PjGCYvedFQo4DXKH69mHfaBE8KVh5lWyBP0ZEMFADDzu97lSAk5/M6PvCSQ8+hGYjaVAstEMTglJY2Mnx/f161QntrSf8c8neRxzNdzplW8r5oj7kmamMvsZGdH7b0VyXij6Hlocbgh7LNzj2lOzZ4pyIX3lHA9C6omR4unXUkn/KZJ71dHa7vf990p3SO5A3Qac8vZnOvuzpiavH62+f1/1nIogyhC9iBjTJyGuV1Z7aeyhgOgn40iGPfdlidgwmyLx7jX11mOWsVYxnpKeElJPDGsgBQvDlNwGvR2grxxKsuE0vFJh+bCIHTspXsyqKDfQy5bbgFL9LJUd1aUXmdm0P2CJjyHL2gtV4r7KI2WK8TPXLSB6ePr83ixasm0fydyiLm1p89DkTO2qIyYkrogvwSwS4UtNcpsO43YI2XdLZKQH69c4HDxrpnCukOEPWq0cLlmWn5DJcVKG32axePSHU+8PK6MtyF9DikBa1ElBdSs6Jkh8LASpi8KSJ/0FlB3opNCmBiQhZlgs8rC1uVqILGm+cxcS06aRog9djOcWvELian/6jZyHmhqlh56YAUVsqW+M/jISkuUl73C74XRbG4/Ed/f04HshJPuycPDBm3pKfTGclocOzMhYELEmacYlTyRLlCjmMVFexNdyNkIFtArpYKMDEK+V5dxg8Xjyk2JIW5JJsi4tBMeMjwNzzGiyju5F8frtYE0cK7obr29YSJYT05M1Y0IJTulj3oV2u6b9RydO8lwu3t9JnsUREcRupNs4+1gahPc/pLOk/n7Mi1/Q3AAHDFVggUufOvKpeSzu9iz6SNxGkma9v1K06fD48Y+LRIuO1FYSn1tvTEHaGrJE54RmcdY+X2CFCGViKxGCRWzEZqfT50keJXLNttn4mk59X6/gNWi9jx3IV+q+zEYOdLAmi9SeIihHMHMjlAiG5MVCEDO5tDj0NSaYGAdR6lIESaPXIcGsm2Ay8aiHfc2oLGfGlyeGAf+h9sIWklArY6J7osdh6U/EgFk9Y8uliXyfUsbLe/AFjTzror+NmrtAVgSqgSVN5PKFdW1LhRihcUrHcWpfz3UcgUu8A1qNETBIgJNq6+FV7n8BBgACbCaQ0Z+YewAAAABJRU5ErkJggg==',
    alt: "picture",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37
    },
    __self: undefined
  }))), __jsx("div", {
    class: "sc-fBuWsC dXbTGN",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: undefined
  }, __jsx("img", {
    src: _images_mobile_png__WEBPACK_IMPORTED_MODULE_2___default.a,
    className: "img-fluid",
    width: "100%",
    alt: "logo",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: undefined
  })), __jsx("div", {
    className: "container",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41
    },
    __self: undefined
  }, __jsx("div", {
    className: "row",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42
    },
    __self: undefined
  }, __jsx("div", {
    className: "col-lg-4 col-md-4 col-10",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: undefined
  }, __jsx("h1", {
    className: "sc-jKJlTe johnthan",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44
    },
    __self: undefined
  }, "Jonathon, Wellcome to"), __jsx("h2", {
    className: "update",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44
    },
    __self: undefined
  }, "Update your profile picture and create a   "))), __jsx("div", {
    className: "container",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47
    },
    __self: undefined
  }, __jsx("div", {
    className: "row",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48
    },
    __self: undefined
  }, __jsx("div", {
    className: "col-lg-4 col-md-4 col-sm-4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49
    },
    __self: undefined
  }, __jsx("input", {
    type: "password",
    className: " fullname",
    id: "email",
    placeholder: "Create Password",
    name: "email/",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50
    },
    __self: undefined
  })), __jsx("div", {
    className: "col-lg-4 col-md-4 col-sm-4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52
    },
    __self: undefined
  }, __jsx("input", {
    type: "text",
    className: " fullname",
    id: "email",
    placeholder: "Mobile Number (optional)",
    name: "email/",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53
    },
    __self: undefined
  }))), __jsx("div", {
    className: "row",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56
    },
    __self: undefined
  }, __jsx("div", {
    className: "col-lg-4 col-md-4 col-sm-4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57
    },
    __self: undefined
  }, __jsx("h3", {
    className: "teal",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58
    },
    __self: undefined
  }, "Teal is gender inclusive "), __jsx("input", {
    type: "text",
    className: " fullname",
    id: "email",
    placeholder: "How Do you Identify?",
    name: "email/",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59
    },
    __self: undefined
  })), __jsx("div", {
    className: "col-lg-4 col-md-4 col-sm-4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62
    },
    __self: undefined
  }, __jsx("h3", {
    className: "teal",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 63
    },
    __self: undefined
  }, "Date of Birth-Should be older "), __jsx("div", {
    className: "col-lg-2 col-md-2 col-sm-2",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 64
    },
    __self: undefined
  }, __jsx("input", {
    type: "text ",
    placeholder: "Day",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65
    },
    __self: undefined
  })), __jsx("div", {
    className: "col-lg-1 col-md-1 col-sm-1",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 67
    },
    __self: undefined
  }, __jsx("input", {
    type: "text",
    placeholder: "Day",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 68
    },
    __self: undefined
  })), __jsx("div", {
    className: "col-lg-1 col-md-1 col-sm-1",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 70
    },
    __self: undefined
  }, __jsx("input", {
    type: "text",
    placeholder: "Day",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71
    },
    __self: undefined
  })))), __jsx("div", {
    className: "row",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75
    },
    __self: undefined
  }, __jsx("div", {
    className: "col-lg-6 col-md-4 col-sm-2",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76
    },
    __self: undefined
  }, __jsx("button", {
    type: "button",
    className: "btn btn-primary btn-lg btn-bloc stepInside ",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 77
    },
    __self: undefined
  }, "Finish Step"))))), __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 82
    },
    __self: undefined
  })))))));
};

index2.getInitialProps = async () => {
  const data = await isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_7___default()('https://jsonplaceholder.typicode.com/todos/1');
  const result = await data.json();
  return {
    posts: result
  };
};

/* harmony default export */ __webpack_exports__["default"] = (index2);

/***/ }),

/***/ 4:
/*!*******************************!*\
  !*** multi ./pages/index2.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\xampp\htdocs\design\pages\index2.js */"./pages/index2.js");


/***/ }),

/***/ "isomorphic-unfetch":
/*!*************************************!*\
  !*** external "isomorphic-unfetch" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("isomorphic-unfetch");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ })

/******/ });
//# sourceMappingURL=index2.js.map